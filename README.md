om: ODT Merge

## Requirements

* node version >= 5.10

## API

Om accepts `POST` `multipart/form-data` requests to `/` with the fields.  An example of a valid `curl` command can be found in [`example_curl.md`](example_curl.md).

### Parameters:
* `odt`: odt file with "@tag@" styles tags to be merged with the `tags` field below
* `tags`: a JSON string of the following form:

  ```json
  {
    "@first_tag@": "Value to merge into instances of '@first_tag@' in document.",
    "@second_tag@": "Value to merge into instances of '@second_tag@' in the document."
  }
  ```

### Returns:
* `status`: 200 OK
* `body`: an ODT file whose `@tags@` have been merged (replaced) with the tag in the `tags` JSON body

## Running locally
Be sure to have [node] (>= 5.10) and [npm] installed, and run:
* `npm install`
* `npm run demo`

### Runing tests
* `npm install`
* `npm run test`

### Features
The best feature of Om is its speed.  It is run through on continuous pipe from request to response.  See the output of `npm test`, and notice how the TEST CLIENT is able to processes files of the output before SERVER has finished unziping the input ODT.

    TEST CLIENT: creating file stream
    TEST CLIENT: making post
    SERVER: found tags: {"first_name":"Tom","last_name":"Dunlap"}
    SERVER: Extracting uploaded file...
    SERVER: Adding mimetype to output odt...
    SERVER: Adding Thumbnails/thumbnail.png to output odt...
    TEST CLIENT: Processing: mimetype
    SERVER: found content.xml, replacing tags (if any)...
    SERVER: setting stream to replace @first_name@ with Tom
    SERVER: setting stream to replace @last_name@ with Dunlap
    SERVER: Adding content.xml to output odt...
    TEST CLIENT: Processing: Thumbnails/thumbnail.png
    SERVER: Adding styles.xml to output odt...
    TEST CLIENT: Processing: content.xml
    SERVER: Adding settings.xml to output odt...
    ok 2 response ODT has expected content
    TEST CLIENT: Processing: styles.xml
    SERVER: Adding meta.xml to output odt...
    TEST CLIENT: Processing: settings.xml
    SERVER: Adding manifest.rdf to output odt...
    TEST CLIENT: Processing: meta.xml
    SERVER: Ignoring Configurations2/floater/...
    SERVER: Ignoring Configurations2/menubar/...
    TEST CLIENT: Processing: manifest.rdf
    SERVER: Ignoring Configurations2/toolbar/...
    SERVER: Ignoring Configurations2/progressbar/...
    SERVER: Ignoring Configurations2/images/Bitmaps/...
    SERVER: Ignoring Configurations2/popupmenu/...
    SERVER: Adding Configurations2/accelerator/current.xml to output odt...
    TEST CLIENT: Processing: Configurations2/accelerator/current.xml
    SERVER: Ignoring Configurations2/toolpanel/...
    SERVER: Ignoring Configurations2/statusbar/...
    SERVER: Adding META-INF/manifest.xml to output odt...
    TEST CLIENT: Processing: META-INF/manifest.xml
    SERVER: input odt EOF reached; finalizing output...

## License
(c) Copywrite CanDo and Tom Dunlap, 2016.  All rights reserved.

[node]: https://nodejs.org/en/
[npm]: https://docs.npmjs.com/getting-started/installing-node
