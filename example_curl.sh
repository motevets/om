#!/usr/bin/env bash

curl \
  -F 'tags={ "first_name": "John", "last_name": "Malkovich" }' \
  -F "odt=@in.odt" \
  localhost:3000 > out.odt

if [[ $? -ne 0 ]]; then
  echo "Looks like there was trouble connecting to the server.  Did you run 'npm run demo' first?"
fi
