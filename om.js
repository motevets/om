"use strict";
const express = require('express');
const Busboy = require('busboy');
const unzip = require('node-unzip-2');
const archiver = require('archiver');
const Promise = require('bluebird');
const replaceStream = require('replacestream');

const app = express();

function log(message) {
  console.log(`SERVER: ${message}`);
}

app.post('/', (req, res) => {
  res.contentType("application/vnd.oasis.opendocument.text");
  const busboy = new Busboy({ headers: req.headers });
  const outOdt = new archiver.create('zip', {});
  const tagsFromRequest = new Promise((resolve) => {
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
      if(fieldname == 'tags'){
        log(`found tags: ${val}`);
        resolve(JSON.parse(val));
      }
    });
    busboy.on('finish', () => {
      if(!tagsFromRequest.isFulfilled()){
        log("No tags were sent.");
        resolve({});
      }
    });
  });
  busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    log('Extracting uploaded file...');
    file.pipe(unzip.Parse())
      .on('entry', (entry) => {
        const entryPath = entry.path;
        const isDir = entry.type == 'Directory';
        let fileStream = entry;

        if(isDir) {
          log(`Ignoring ${entryPath}...`);
          fileStream.autodrain();
        } else {
          if(entryPath == 'content.xml') {
            const tags = tagsFromRequest.value();
            log('found content.xml, replacing tags (if any)...');
            for (const tagName in tags){
              log(`setting stream to replace @${tagName}@ with ${tags[tagName]}`);
              fileStream = fileStream.pipe(replaceStream(`@${tagName}@`, tags[tagName]));
            }
          }
          log(`Adding ${entryPath} to output odt...`);
          outOdt.append(fileStream, { name: entryPath });
        }
      })
    .on('close', () => {
      log('input odt EOF reached; finalizing output...');
      outOdt.finalize();
    });
  });
  req.pipe(busboy);
  outOdt.pipe(res);
});

const server = app.listen(3000, function () {
  log('om demo server listening on port 3000!');
});

exports.server = server;
