"use strict";
let test = require('tape');
let request = require('request');
let fs = require('fs');
let unzip = require('node-unzip-2');
let om = require('./om');

const api_url = 'http://localhost:3000/';
const preMergedContent = 'Hello @first_name@ @last_name@!';
const postMergedContent = 'Hello Tom Dunlap!';

function log(message) {
  console.log(`TEST CLIENT: ${message}`);
}

test('Sending an ODT with no tags returns original file', { timeout: 5000 }, (t) => {
  let inOdt = fs.createReadStream(`${__dirname}/in.odt`)
  let formData = {
    odt: inOdt
  }
  request.post({url: api_url, formData: formData})
    .pipe(unzip.Parse())
    .on('entry', (entry) => {
      log('Processing: '+entry.path);
      if (entry.path == 'content.xml') {
        let content = "";
        entry
          .on('data', (buffer) => { content += buffer.toString() })
          .on('end', () => {
            t.assert((content.indexOf(preMergedContent) >= 0), 'response ODT has expected content');
          });
      }
      entry.autodrain();
    })
    .on('close', () => {
      log('closing input odt');
      inOdt.close();
      t.end();
    });
});

test('Sending an ODT with replaces tags', { timeout: 5000 }, (t) => {
  log('creating file stream');
  let formData = {
    tags: JSON.stringify({ 'first_name': 'Tom', 'last_name': 'Dunlap' }),
    odt: fs.createReadStream(`${__dirname}/in.odt`)
  }
  log('making post');
  request.post({url: api_url, formData: formData})
    .pipe(unzip.Parse())
    .on('entry', (entry) => {
      log('Processing: '+entry.path);
      if (entry.path == 'content.xml') {
        let content = "";
        entry
          .on('data', (buffer) => { content += buffer.toString() })
          .on('end', () => {
            t.assert((content.indexOf(postMergedContent) >= 0), 'response ODT has expected content');
            t.end();
          });
      }
      entry.autodrain();
    });
});

test.onFinish( () => {
  om.server.close();
});
